# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_07_133026) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "accounts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.string "full_name"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "postal_code"
    t.string "state_province"
    t.string "country"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["community_id"], name: "index_accounts_on_community_id"
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "action_flows", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "event_type"
    t.string "event_condition"
    t.json "event_action"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "community_id"
    t.string "event_condition_query"
    t.string "status", default: "not_deleted"
    t.index ["community_id"], name: "index_action_flows_on_community_id"
    t.index ["title"], name: "index_action_flows_on_title", unique: true
  end

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activity_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_id"
    t.uuid "reporting_user_id"
  end

  create_table "activity_points", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "article_read", default: 0
    t.integer "comment", default: 0
    t.integer "login", default: 0
    t.integer "referral", default: 0
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "article_shared", default: 0
    t.index ["user_id"], name: "index_activity_points_on_user_id"
  end

  create_table "assignee_notes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "note_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "reminder_time"
    t.string "reminder_job_id"
    t.index ["note_id"], name: "index_assignee_notes_on_note_id"
    t.index ["user_id", "note_id"], name: "index_assignee_notes_on_user_id_and_note_id", unique: true
    t.index ["user_id"], name: "index_assignee_notes_on_user_id"
  end

  create_table "businesses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.string "name"
    t.string "status"
    t.string "home_url"
    t.string "category"
    t.text "description"
    t.string "image_url"
    t.string "email"
    t.string "phone_number"
    t.string "address"
    t.string "operation_hours"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "links"
    t.index ["community_id"], name: "index_businesses_on_community_id"
    t.index ["user_id"], name: "index_businesses_on_user_id"
  end

  create_table "campaign_labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "campaign_id", null: false
    t.uuid "label_id", null: false
    t.index ["campaign_id", "label_id"], name: "index_campaign_labels_on_campaign_id_and_label_id", unique: true
    t.index ["campaign_id"], name: "index_campaign_labels_on_campaign_id"
    t.index ["label_id"], name: "index_campaign_labels_on_label_id"
  end

  create_table "campaigns", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id", null: false
    t.string "name"
    t.string "message"
    t.text "user_id_list"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "batch_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "total_sent"
    t.integer "total_clicked"
    t.string "campaign_type", default: "sms", null: false
    t.string "subject"
    t.string "pre_header"
    t.string "template_style"
    t.integer "status", default: 0
    t.integer "message_count", default: 0
    t.boolean "include_reply_link", default: false
    t.uuid "email_templates_id"
    t.index ["campaign_type"], name: "index_campaigns_on_campaign_type"
    t.index ["community_id", "status"], name: "index_campaigns_on_community_id_and_status"
    t.index ["community_id"], name: "index_campaigns_on_community_id"
    t.index ["email_templates_id"], name: "index_campaigns_on_email_templates_id"
  end

  create_table "comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.text "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "discussion_id"
    t.string "status"
    t.uuid "community_id"
    t.index ["community_id"], name: "index_comments_on_community_id"
    t.index ["status"], name: "index_comments_on_status"
  end

  create_table "communities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "google_domain"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "slug"
    t.string "logo_url"
    t.string "slack_webhook_url"
    t.string "timezone"
    t.string "default_users", default: [], array: true
    t.json "templates"
    t.string "hostname"
    t.json "support_number"
    t.json "support_email"
    t.json "support_whatsapp"
    t.string "currency"
    t.string "locale"
    t.string "tagline"
    t.string "language"
    t.string "wp_link"
    t.json "features"
    t.json "theme_colors"
    t.string "security_manager"
    t.json "social_links"
    t.json "banking_details"
    t.json "community_required_fields"
    t.index ["slug"], name: "index_communities_on_slug", unique: true
  end

  create_table "contact_infos", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "contact_type"
    t.string "info"
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_contact_infos_on_user_id"
  end

  create_table "discussion_users", force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "discussion_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["discussion_id"], name: "index_discussion_users_on_discussion_id"
    t.index ["user_id", "discussion_id"], name: "index_discussion_users_on_user_id_and_discussion_id", unique: true
    t.index ["user_id"], name: "index_discussion_users_on_user_id"
  end

  create_table "discussions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.text "description"
    t.string "title"
    t.string "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "valid"
    t.index ["community_id"], name: "index_discussions_on_community_id"
    t.index ["status"], name: "index_discussions_on_status"
    t.index ["user_id"], name: "index_discussions_on_user_id"
  end

  create_table "email_templates", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "subject"
    t.text "body"
    t.uuid "community_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "template_variables"
    t.json "data"
    t.string "tag"
    t.index ["community_id"], name: "index_email_templates_on_community_id"
    t.index ["name", "community_id"], name: "index_email_templates_on_name_and_community_id", unique: true
  end

  create_table "entry_requests", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "community_id"
    t.string "name"
    t.string "nrc"
    t.string "phone_number"
    t.string "vehicle_plate"
    t.string "reason"
    t.string "other_reason"
    t.boolean "concern_flag"
    t.integer "granted_state"
    t.uuid "grantor_id"
    t.datetime "granted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "source"
    t.boolean "acknowledged"
    t.datetime "visitation_date"
    t.string "start_time"
    t.string "end_time"
    t.string "company_name"
  end

  create_table "event_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id"
    t.uuid "acting_user_id"
    t.uuid "ref_id"
    t.string "ref_type"
    t.string "subject"
    t.json "data"
    t.datetime "created_at"
    t.index ["ref_id"], name: "index_event_logs_on_ref_id"
  end

  create_table "feedbacks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "user_id"
    t.boolean "is_thumbs_up"
    t.datetime "date"
    t.string "review"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "community_id"
    t.index ["community_id"], name: "index_feedbacks_on_community_id"
  end

  create_table "form_properties", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "order"
    t.string "field_name"
    t.integer "field_type"
    t.boolean "required", default: false
    t.string "short_desc"
    t.string "long_desc"
    t.boolean "admin_use", default: false
    t.uuid "form_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "field_value"
    t.index ["form_id"], name: "index_form_properties_on_form_id"
  end

  create_table "form_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "form_id", null: false
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "status_updated_by_id"
    t.index ["form_id"], name: "index_form_users_on_form_id"
    t.index ["status_updated_by_id"], name: "index_form_users_on_status_updated_by_id"
    t.index ["user_id"], name: "index_form_users_on_user_id"
  end

  create_table "forms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "community_id", null: false
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: 0
    t.text "description"
    t.boolean "multiple_submissions_allowed"
    t.index ["community_id"], name: "index_forms_on_community_id"
    t.index ["name"], name: "index_forms_on_name", unique: true
  end

  create_table "import_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "file_name"
    t.boolean "failed", default: false
    t.json "import_errors"
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["community_id"], name: "index_import_logs_on_community_id"
    t.index ["user_id"], name: "index_import_logs_on_user_id"
  end

  create_table "invoices", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "land_parcel_id", null: false
    t.uuid "community_id", null: false
    t.datetime "due_date"
    t.decimal "amount", precision: 11, scale: 2
    t.integer "status"
    t.string "description"
    t.string "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_id"
    t.uuid "created_by_id"
    t.decimal "pending_amount", precision: 11, scale: 2
    t.integer "invoice_number"
    t.boolean "autogenerated", default: false
    t.uuid "payment_plan_id"
    t.index ["community_id"], name: "index_invoices_on_community_id"
    t.index ["created_by_id"], name: "index_invoices_on_created_by_id"
    t.index ["land_parcel_id"], name: "index_invoices_on_land_parcel_id"
    t.index ["payment_plan_id"], name: "index_invoices_on_payment_plan_id"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "short_desc"
    t.uuid "community_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.string "color", default: "#f07030"
    t.string "status", default: "active"
    t.index ["community_id"], name: "index_labels_on_community_id"
  end

  create_table "land_parcel_accounts", id: false, force: :cascade do |t|
    t.uuid "land_parcel_id", null: false
    t.uuid "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_land_parcel_accounts_on_account_id"
    t.index ["land_parcel_id", "account_id"], name: "index_land_parcel_accounts_on_land_parcel_id_and_account_id", unique: true
    t.index ["land_parcel_id"], name: "index_land_parcel_accounts_on_land_parcel_id"
  end

  create_table "land_parcels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "community_id", null: false
    t.string "parcel_number"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "postal_code"
    t.string "state_province"
    t.string "country"
    t.string "parcel_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "long_x", precision: 10, scale: 6
    t.decimal "lat_y", precision: 10, scale: 6
    t.json "geom"
    t.integer "deleted_status", default: 0
    t.boolean "is_poi", default: false
    t.index ["community_id"], name: "index_land_parcels_on_community_id"
    t.index ["deleted_status"], name: "index_land_parcels_on_deleted_status"
    t.index ["parcel_number"], name: "index_land_parcels_on_parcel_number", unique: true
  end

  create_table "messages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "receiver"
    t.text "message"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_id"
    t.uuid "sender_id"
    t.boolean "is_read"
    t.datetime "read_at"
    t.uuid "campaign_id"
    t.string "source_system_id"
    t.string "category"
    t.uuid "note_id"
    t.index ["campaign_id"], name: "index_messages_on_campaign_id"
    t.index ["note_id"], name: "index_messages_on_note_id"
  end

  create_table "note_comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "body"
    t.uuid "note_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.index ["note_id"], name: "index_note_comments_on_note_id"
    t.index ["user_id"], name: "index_note_comments_on_user_id"
  end

  create_table "note_histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "attr_changed"
    t.string "initial_value"
    t.string "updated_value"
    t.string "action"
    t.string "note_entity_type"
    t.uuid "note_entity_id"
    t.uuid "note_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["note_entity_type", "note_entity_id"], name: "index_note_histories_on_note_entity_type_and_note_entity_id"
    t.index ["note_id"], name: "index_note_histories_on_note_id"
    t.index ["user_id"], name: "index_note_histories_on_user_id"
  end

  create_table "notes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "author_id"
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "flagged"
    t.boolean "completed"
    t.datetime "due_date"
    t.string "category"
    t.uuid "assigned_to"
    t.uuid "community_id"
    t.text "description"
    t.uuid "form_user_id"
    t.boolean "autogenerated", default: false
    t.index ["form_user_id"], name: "index_notes_on_form_user_id"
  end

  create_table "notifications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "seen_at"
    t.string "notifable_type"
    t.uuid "notifable_id"
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.uuid "community_id"
    t.index ["community_id"], name: "index_notifications_on_community_id"
    t.index ["notifable_type", "notifable_id"], name: "index_notifications_on_notifable_type_and_notifable_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "payment_invoices", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "invoice_id", null: false
    t.uuid "payment_id", null: false
    t.uuid "wallet_transaction_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["invoice_id"], name: "index_payment_invoices_on_invoice_id"
    t.index ["payment_id", "invoice_id"], name: "index_payment_invoices_on_payment_id_and_invoice_id", unique: true
    t.index ["payment_id"], name: "index_payment_invoices_on_payment_id"
    t.index ["wallet_transaction_id"], name: "index_payment_invoices_on_wallet_transaction_id"
  end

  create_table "payment_plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "land_parcel_id", null: false
    t.string "plan_type"
    t.datetime "start_date"
    t.integer "status"
    t.decimal "percentage", precision: 11, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "generated", default: false
    t.decimal "plot_balance", precision: 11, scale: 2, default: "0.0"
    t.decimal "total_amount", precision: 11, scale: 2
    t.integer "duration_in_month"
    t.decimal "pending_balance", precision: 11, scale: 2, default: "0.0"
    t.decimal "monthly_amount", precision: 11, scale: 2
    t.integer "payment_day", default: 1
    t.index ["land_parcel_id"], name: "index_payment_plans_on_land_parcel_id"
    t.index ["user_id"], name: "index_payment_plans_on_user_id"
  end

  create_table "payments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "invoice_id"
    t.string "payment_type"
    t.decimal "amount", precision: 11, scale: 2
    t.integer "payment_status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "bank_name"
    t.string "cheque_number"
    t.uuid "community_id", default: "ec7625ee-0bfe-4dcb-9a37-831fc77fa302", null: false
    t.index ["community_id"], name: "index_payments_on_community_id"
    t.index ["invoice_id"], name: "index_payments_on_invoice_id"
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "plan_payments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.decimal "amount"
    t.integer "status"
    t.uuid "transaction_id", null: false
    t.uuid "user_id", null: false
    t.uuid "community_id", null: false
    t.uuid "payment_plan_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "manual_receipt_number"
    t.integer "automated_receipt_number"
    t.index ["community_id"], name: "index_plan_payments_on_community_id"
    t.index ["manual_receipt_number", "community_id"], name: "index_plan_payments_on_manual_receipt_number_and_community_id", unique: true
    t.index ["payment_plan_id"], name: "index_plan_payments_on_payment_plan_id"
    t.index ["transaction_id"], name: "index_plan_payments_on_transaction_id"
    t.index ["user_id"], name: "index_plan_payments_on_user_id"
  end

  create_table "post_tag_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "post_tag_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_tag_id"], name: "index_post_tag_users_on_post_tag_id"
    t.index ["user_id", "post_tag_id"], name: "index_post_tag_users_on_user_id_and_post_tag_id", unique: true
    t.index ["user_id"], name: "index_post_tag_users_on_user_id"
  end

  create_table "post_tags", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "community_id"
    t.index ["community_id"], name: "index_post_tags_on_community_id"
    t.index ["name"], name: "index_post_tags_on_name", unique: true
  end

  create_table "showrooms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "userId"
    t.string "name"
    t.string "email"
    t.string "home_address"
    t.string "phone_number"
    t.string "nrc"
    t.string "reason"
    t.string "source"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "substatus_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "stop_date"
    t.string "previous_status"
    t.string "new_status"
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["community_id"], name: "index_substatus_logs_on_community_id"
    t.index ["user_id"], name: "index_substatus_logs_on_user_id"
  end

  create_table "time_sheets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "shift_start_event_log_id"
    t.uuid "shift_end_event_log_id"
    t.uuid "user_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["shift_end_event_log_id"], name: "index_time_sheets_on_shift_end_event_log_id"
    t.index ["shift_start_event_log_id"], name: "index_time_sheets_on_shift_start_event_log_id"
    t.index ["user_id"], name: "index_time_sheets_on_user_id"
  end

  create_table "transactions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "source"
    t.integer "status"
    t.decimal "amount"
    t.string "receipt_number"
    t.datetime "originally_created_at"
    t.string "transaction_number"
    t.string "cheque_number"
    t.string "bank_name"
    t.uuid "depositor_id"
    t.uuid "community_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["community_id"], name: "index_transactions_on_community_id"
    t.index ["depositor_id"], name: "index_transactions_on_depositor_id"
    t.index ["transaction_number"], name: "index_transactions_on_transaction_number", unique: true
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "user_form_properties", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "form_property_id", null: false
    t.uuid "form_user_id", null: false
    t.uuid "user_id"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["form_property_id"], name: "index_user_form_properties_on_form_property_id"
    t.index ["form_user_id"], name: "index_user_form_properties_on_form_user_id"
    t.index ["user_id"], name: "index_user_form_properties_on_user_id"
  end

  create_table "user_labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "label_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["label_id"], name: "index_user_labels_on_label_id"
    t.index ["user_id", "label_id"], name: "index_user_labels_on_user_id_and_label_id", unique: true
    t.index ["user_id"], name: "index_user_labels_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "provider"
    t.string "uid"
    t.string "token"
    t.boolean "oauth_expires"
    t.datetime "oauth_expires_at"
    t.string "refresh_token"
    t.string "image_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "phone_number"
    t.string "phone_token"
    t.datetime "phone_token_expires_at"
    t.string "user_type"
    t.string "request_reason"
    t.string "request_status"
    t.text "request_note"
    t.string "vehicle"
    t.uuid "community_id"
    t.datetime "last_activity_at"
    t.string "state"
    t.datetime "expires_at"
    t.string "source"
    t.string "stage"
    t.uuid "owner_id"
    t.string "id_number"
    t.datetime "followup_at"
    t.integer "sub_status"
    t.string "address"
    t.uuid "latest_substatus_id"
    t.string "ext_ref_id"
    t.index ["community_id", "email"], name: "index_users_on_community_id_and_email", unique: true
    t.index ["latest_substatus_id"], name: "index_users_on_latest_substatus_id"
    t.index ["sub_status"], name: "index_users_on_sub_status"
    t.index ["uid", "provider", "community_id"], name: "index_users_on_uid_and_provider_and_community_id", unique: true
  end

  create_table "valuations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.date "start_date"
    t.decimal "amount", precision: 11, scale: 2
    t.uuid "land_parcel_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["land_parcel_id"], name: "index_valuations_on_land_parcel_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.uuid "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "wallet_transactions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "source"
    t.string "destination"
    t.decimal "amount", precision: 11, scale: 2
    t.integer "status"
    t.string "bank_name"
    t.string "cheque_number"
    t.decimal "current_wallet_balance", precision: 11, scale: 2
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "community_id", default: "ec7625ee-0bfe-4dcb-9a37-831fc77fa302", null: false
    t.string "transaction_number"
    t.uuid "depositor_id"
    t.string "receipt_number"
    t.datetime "originally_created_at"
    t.uuid "payment_plan_id"
    t.json "settled_invoices"
    t.decimal "current_pending_plot_balance", precision: 11, scale: 2
    t.index ["community_id"], name: "index_wallet_transactions_on_community_id"
    t.index ["depositor_id"], name: "index_wallet_transactions_on_depositor_id"
    t.index ["payment_plan_id"], name: "index_wallet_transactions_on_payment_plan_id"
    t.index ["transaction_number"], name: "index_wallet_transactions_on_transaction_number", unique: true
    t.index ["user_id"], name: "index_wallet_transactions_on_user_id"
  end

  create_table "wallets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.string "currency"
    t.decimal "balance", precision: 11, scale: 2
    t.decimal "pending_balance", precision: 11, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "unallocated_funds", precision: 11, scale: 2, default: "0.0"
    t.index ["user_id"], name: "index_wallets_on_user_id"
  end

  add_foreign_key "accounts", "communities"
  add_foreign_key "accounts", "users"
  add_foreign_key "action_flows", "communities"
  add_foreign_key "activity_points", "users"
  add_foreign_key "assignee_notes", "notes"
  add_foreign_key "assignee_notes", "users"
  add_foreign_key "businesses", "communities"
  add_foreign_key "businesses", "users"
  add_foreign_key "campaign_labels", "campaigns"
  add_foreign_key "campaign_labels", "labels"
  add_foreign_key "campaigns", "communities"
  add_foreign_key "campaigns", "email_templates", column: "email_templates_id"
  add_foreign_key "comments", "communities"
  add_foreign_key "contact_infos", "users"
  add_foreign_key "discussion_users", "discussions"
  add_foreign_key "discussion_users", "users"
  add_foreign_key "discussions", "communities"
  add_foreign_key "discussions", "users"
  add_foreign_key "email_templates", "communities"
  add_foreign_key "feedbacks", "communities"
  add_foreign_key "form_properties", "forms"
  add_foreign_key "form_users", "forms"
  add_foreign_key "form_users", "users"
  add_foreign_key "form_users", "users", column: "status_updated_by_id"
  add_foreign_key "forms", "communities"
  add_foreign_key "import_logs", "communities"
  add_foreign_key "import_logs", "users"
  add_foreign_key "invoices", "communities"
  add_foreign_key "invoices", "land_parcels"
  add_foreign_key "invoices", "payment_plans"
  add_foreign_key "invoices", "users"
  add_foreign_key "labels", "communities"
  add_foreign_key "land_parcel_accounts", "accounts"
  add_foreign_key "land_parcel_accounts", "land_parcels"
  add_foreign_key "land_parcels", "communities"
  add_foreign_key "note_comments", "notes"
  add_foreign_key "note_comments", "users"
  add_foreign_key "note_histories", "notes"
  add_foreign_key "note_histories", "users"
  add_foreign_key "notes", "form_users"
  add_foreign_key "notifications", "communities"
  add_foreign_key "notifications", "users"
  add_foreign_key "payment_invoices", "invoices"
  add_foreign_key "payment_invoices", "payments"
  add_foreign_key "payment_invoices", "wallet_transactions"
  add_foreign_key "payment_plans", "land_parcels"
  add_foreign_key "payment_plans", "users"
  add_foreign_key "payments", "communities"
  add_foreign_key "payments", "invoices"
  add_foreign_key "payments", "users"
  add_foreign_key "plan_payments", "communities"
  add_foreign_key "plan_payments", "payment_plans"
  add_foreign_key "plan_payments", "transactions"
  add_foreign_key "plan_payments", "users"
  add_foreign_key "post_tag_users", "post_tags"
  add_foreign_key "post_tag_users", "users"
  add_foreign_key "post_tags", "communities"
  add_foreign_key "substatus_logs", "communities"
  add_foreign_key "substatus_logs", "users"
  add_foreign_key "transactions", "communities"
  add_foreign_key "transactions", "users"
  add_foreign_key "user_form_properties", "form_properties"
  add_foreign_key "user_form_properties", "form_users"
  add_foreign_key "user_form_properties", "users"
  add_foreign_key "user_labels", "labels"
  add_foreign_key "user_labels", "users"
  add_foreign_key "valuations", "land_parcels"
  add_foreign_key "wallet_transactions", "communities"
  add_foreign_key "wallet_transactions", "payment_plans"
  add_foreign_key "wallet_transactions", "users"
  add_foreign_key "wallets", "users"
end
