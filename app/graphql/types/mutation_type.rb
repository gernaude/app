# frozen_string_literal: true

module Types
  # MutationType
  class MutationType < Types::BaseObject
    field :activity_log_add, mutation: Mutations::ActivityLog::Add
    field :activity_log_update_log, mutation: Mutations::ActivityLog::UpdateLog
    field :user_create, mutation: Mutations::User::Create
    field :user_update, mutation: Mutations::User::Update
    field :one_time_login, mutation: Mutations::User::OneTimeLogin
    field :user_merge, mutation: Mutations::User::Merge
    field :create_upload, mutation: Mutations::Upload::CreateAttachment
    field :users_import, mutation: Mutations::User::Import

    # Entry Requests
    field :entry_request_create, mutation: Mutations::EntryRequest::EntryRequestCreate
    field :entry_request_update, mutation: Mutations::EntryRequest::EntryRequestUpdate
    field :entry_request_grant, mutation: Mutations::EntryRequest::EntryRequestGrant
    field :entry_request_deny, mutation: Mutations::EntryRequest::EntryRequestDeny
    field :entry_request_acknowledge, mutation: Mutations::EntryRequest::EntryRequestAcknowledge
    field :entry_request_note, mutation: Mutations::EntryRequest::EntryRequestNote

    # User login
    field :login_phone_start, mutation: Mutations::Login::LoginPhoneStart
    field :login_phone_complete, mutation: Mutations::Login::LoginPhoneComplete
    field :login_switch_user, mutation: Mutations::Login::LoginSwitchUser

    # Notes
    field :note_create, mutation: Mutations::Note::NoteCreate
    field :note_update, mutation: Mutations::Note::NoteUpdate
    field :note_assign, mutation: Mutations::Note::NoteAssign
    field :note_comment_create, mutation: Mutations::Note::NoteCommentCreate
    field :note_comment_update, mutation: Mutations::Note::NoteCommentUpdate
    field :note_comment_delete, mutation: Mutations::Note::NoteCommentDelete
    field :note_bulk_update, mutation: Mutations::Note::NoteBulkUpdate
    field :set_note_reminder, mutation: Mutations::Note::SetNoteReminder

    # Feedback
    field :feedback_create, mutation: Mutations::Feedback::FeedbackCreate

    # showroom
    field :showroom_entry_create, mutation: Mutations::Showroom::ShowroomCreate

    # messages
    field :message_create, mutation: Mutations::Message::MessageCreate
    field :message_notification_update, mutation: Mutations::Message::MessageNotificationUpdate

    # temperature capture
    field :temperature_update, mutation: Mutations::Temperature::TemperatureUpdate

    # track time
    field :manage_shift, mutation: Mutations::Timesheet::ManageShift

    # campaigns
    field :campaign_create, mutation: Mutations::Campaign::CampaignCreate
    field :campaign_update, mutation: Mutations::Campaign::CampaignUpdate
    field :campaign_delete, mutation: Mutations::Campaign::CampaignDelete
    field :campaign_create_through_users, mutation: Mutations::Campaign::CampaignCreateThroughUsers
    field :campaign_label_remove, mutation: Mutations::Campaign::CampaignLabelRemove

    # comments
    field :comment_create, mutation: Mutations::Comment::CommentCreate
    field :comment_update, mutation: Mutations::Comment::CommentUpdate

    # discussions
    field :discussion_create, mutation: Mutations::Discussion::DiscussionCreate
    field :discussion_update, mutation: Mutations::Discussion::DiscussionUpdate
    field :discussion_user_create, mutation: Mutations::Discussion::DiscussionUserCreate

    # labels
    field :label_create, mutation: Mutations::Label::LabelCreate
    field :label_update, mutation: Mutations::Label::LabelUpdate
    field :label_delete, mutation: Mutations::Label::LabelDelete
    field :user_label_create, mutation: Mutations::Label::UserLabelCreate
    field :user_label_update, mutation: Mutations::Label::UserLabelUpdate
    field :label_merge, mutation: Mutations::Label::LabelMerge

    # notifications
    field :notification_preference, mutation: Mutations::Settings::NotificationPreference

    # businesses
    field :business_create, mutation: Mutations::Business::BusinessCreate
    # Businesses
    field :business_delete, mutation: Mutations::Business::BusinessDelete

    # posts
    field :log_read_post, mutation: Mutations::Post::LogReadPost
    field :log_shared_post, mutation: Mutations::Post::LogSharedPost
    field :follow_post_tag, mutation: Mutations::Post::FollowPostTag

    # forms
    field :form_create, mutation: Mutations::Form::FormCreate
    field :form_update, mutation: Mutations::Form::FormUpdate
    field :form_properties_create, mutation: Mutations::Form::FormPropertiesCreate
    field :form_properties_update, mutation: Mutations::Form::FormPropertiesUpdate
    field :form_properties_delete, mutation: Mutations::Form::FormPropertiesDelete
    field :form_user_create, mutation: Mutations::Form::FormUserCreate
    field :form_user_update, mutation: Mutations::Form::FormUserUpdate
    field :form_user_status_update, mutation: Mutations::Form::FormUserStatusUpdate
    field :user_form_properties_create, mutation: Mutations::Form::UserFormPropertiesCreate
    field :user_form_properties_update, mutation: Mutations::Form::UserFormPropertiesUpdate

    # land_parcel
    field :PropertyCreate, mutation: Mutations::LandParcel::PropertyCreate
    field :property_update, mutation: Mutations::LandParcel::PropertyUpdate
    field :property_merge, mutation: Mutations::LandParcel::PropertyMerge
    field :point_of_interest_create, mutation: Mutations::LandParcel::PointOfInterestCreate
    field :point_of_interest_delete, mutation: Mutations::LandParcel::PointOfInterestDelete
    field :poi_image_upload, mutation: Mutations::LandParcel::PointOfInterestImageCreate

    # action_flow
    field :action_flow_create, mutation: Mutations::ActionFlow::ActionFlowCreate
    field :action_flow_update, mutation: Mutations::ActionFlow::ActionFlowUpdate
    field :action_flow_delete, mutation: Mutations::ActionFlow::ActionFlowDelete

    # community
    field :community_update, mutation: Mutations::Community::CommunityUpdate

    # contact_info
    field :contact_info_delete, mutation: Mutations::ContactInfo::Delete

    # invoice
    field :invoice_create, mutation: Mutations::Invoice::InvoiceCreate
    field :invoice_cancel, mutation: Mutations::Invoice::InvoiceCancel

    # payments
    field :payment_plan_create, mutation: Mutations::Payment::PaymentPlanCreate
    field :payment_day_update, mutation: Mutations::Payment::PaymentDayUpdate
    field :plan_payment_cancel, mutation: Mutations::Payment::PlanPaymentCancel
    field :plan_payment_create, mutation: Mutations::Payment::PlanPaymentCreate

    # transactions
    field :wallet_transaction_create, mutation: Mutations::Transaction::WalletTransactionCreate
    field :wallet_transaction_update, mutation: Mutations::Transaction::WalletTransactionUpdate
    field :wallet_transaction_revert, mutation: Mutations::Transaction::WalletTransactionRevert
    field :transaction_create, mutation: Mutations::Transaction::TransactionCreate
    field :transaction_revert, mutation: Mutations::Transaction::TransactionRevert

    # email_template
    field :email_template_create, mutation: Mutations::EmailTemplate::TemplateCreate
    field :email_template_update, mutation: Mutations::EmailTemplate::TemplateUpdate

    # substatus_logs
    field :substatus_log_update, mutation: Mutations::SubstatusLog::SubstatusLogUpdate
  end
end
