/* eslint-disable */
import { useState, useEffect } from 'react'

/**
 *
 * @param {Number} initialTime
 * @param {Number} delay
 * @description returns remaining time in seconds
 * @returns time in seconds
 */
export default function useTimer(initialTime, delay) {
  const [time, setTime] = useState(initialTime)
  useEffect(() => {
    if (!time) return
    const intervalId = setInterval(() => {
      setTime(time - 1)
    }, delay)
    return () => clearInterval(intervalId)
  }, [delay, time])

  return time
}

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window
  return {
    width,
    height
  }
}

/**
 *
 * @description return current width and height of the window
 */
export function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  )

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions())
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return windowDimensions
}


/**
 * 
 * @param {string} url API endpoint to fetch from
 * @param {object} options include headers and http method here [GET, POST, ...]
 * @returns {object} response and error
 * 
 */
export function useFetch(url) {
  const [response, setData] = useState({});
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await fetch(url);
        const json = await result.json();
        setData(json);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, [url]);
  return { response, error };
}
