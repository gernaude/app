const authState = {
    loaded: true,
    loggedIn: true,
    user: {
      avatarUrl: null,
      community: { name: 'City', logoUrl: 'http://image.jpg' },
      email: 'user@community.co',
      expiresAt: null,
      id: '11cdad78',
      imageUrl: 'image.jpg',
      name: 'John Doctor',
      phoneNumber: '260971500000',
      userType: 'admin'
    }
  }
  export default authState;