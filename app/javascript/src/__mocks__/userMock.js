const userMock = {
    user: {
      id: 'a54d6184-b10e-4865-bee7-7957701d423d',
      token: 'a54d6184-b10e-4865-bee7',
      name: 'Another somebodyy',
      userType: 'client',
      expiresAt: null,
      community: {
        supportName: 'Support Officer',
        themeColors: {
          primaryColor: "#nnn",
          secondaryColor: "#nnn"
        },
        name: 'Another Comm',
        imageUrl: 'http://image.jpg',
        locale: 'en-ZM',
        currency: 'ZMW',
        id: "293849323829891",
        wpLink: "http://link.com"
      }
    }
  };

  export default userMock;