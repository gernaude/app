import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import DeleteDialogue from '../../../shared/dialogs/DeleteDialogue'
import { Spinner } from '../../../shared/Loading';

describe('It should render the dialog box for delete', () => {
    it('It should render with dialog', () => {
      const container = render(
        <DeleteDialogue
          open
          handleClose={jest.fn()}
          handleDelete={jest.fn()}
          title="business"
          handleAction={jest.fn}
          loading={false}
          action="Delete"
        />
      )
      expect(container.queryByTestId('delete_dialog')).toBeInTheDocument()
      expect(container.queryByTestId('confirm_action')).toBeInTheDocument()
    });
    it('It should render loader when confirm action is clicked', () => {
      const container = render(
        <DeleteDialogue
          open
          handleClose={jest.fn()}
          handleDelete={jest.fn()}
          title="business"
          handleAction={jest.fn}
          loading
          action="Delete"
        />
      )
      expect(container.queryByTestId('delete_dialog')).toBeInTheDocument()
      expect(container.queryByTestId('confirm_action')).toBeNull()
      const loader = render(<Spinner />);
      expect(loader.queryAllByTestId('loader')[0]).toBeInTheDocument();
  
    });
});
