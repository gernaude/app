import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DetailHeading from '../../shared/DetailHeading';

describe('It should test the detail heading component', () => {
  it('it should render detail heading', () => {
    const container = render(<DetailHeading title="Heading" />);
    expect(container.queryByText('Heading')).toBeInTheDocument();
  });
});
