import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/react-testing';
import CommunitySettings from '../components/Settings';
import { CommunityUpdateMutation } from '../graphql/community_mutations';
import MockedThemeProvider from '../../__mocks__/mock_theme';

jest.mock('@rails/activestorage/src/file_checksum', () => jest.fn());
describe('Community settings page ', () => {
  it('should have input field and a remove button', async () => {
    const data = {
      supportNumber: [
        {
          phone_number: '260971500748',
          category: 'sales'
        }
      ],
      supportWhatsapp: [
        {
          whatsapp: '260920000748',
          category: 'sales'
        }
      ],
      supportEmail: [
        {
          email: 'abc@gmail.com',
          category: 'customer care'
        },
        {
          email: 'joey@hi.co',
          category: 'sales'
        }
      ],
      socialLinks: [
        { social_link: 'www.facebook.com', category: 'facebook' },
      ],
      logoUrl: null,
      bankingDetails: {
        bankName: 'Test bank name',
        accountName: 'Thebe',
        accountNo: '1234',
        branch: 'Test branch',
        swiftCode: '032',
        sortCode: '456',
        address: '11, Nalikwanda Rd,',
        city: 'Lusaka',
        country: '',
        taxIdNo: '',
      },
    };

    const communityMutationMock = {
      request: {
        query: CommunityUpdateMutation,
        variables: {
          supportNumber: [{ phone_number: '', category: '' }],
          supportEmail: [
            { email: 'abc@gmail.com', category: 'customer care' },
            { email: 'joey@hi.co', category: 'sales' },
            { email: '', category: '' }
          ],
          supportWhatsapp: [
            { whatsapp: '260920000748', category: 'sales' },
            { whatsapp: '', category: '' }
          ],
          socialLinks: [
            { social_link: 'www.facebook.com', category: 'facebook' },
            { social_link: '', category: '' },
          ],
          imageBlobId: null,
          locale: 'en-US',
          tagline: '',
          logoUrl: '',
          wpLink: '',
          securityManager: '',
          themeColors: { primaryColor: '#69ABA4', secondaryColor: '#cf5628' },
          bankingDetails: {
            bankName: 'Test bank name',
            accountName: 'Thebe',
            accountNo: '1234',
            branch: 'Test branch',
            swiftCode: '032',
            sortCode: '456',
            address: '11, Nalikwanda Rd,',
            city: 'Lusaka',
            country: '',
            taxIdNo: '',
          },
        }
      },
      result: {
        data: {
          communityUpdate: {
            id: '11cdad78-5a04-4026-828c-17290a2c44b6'
          }
        }
      }
    };
    const refetchMock = jest.fn();
    const container = render(
      <MockedProvider mocks={[communityMutationMock]}>
        <MockedThemeProvider>
          <CommunitySettings data={data} token="374857uwehfsdf232" refetch={refetchMock} />
        </MockedThemeProvider>
      </MockedProvider>
    );
    expect(container.queryByText('community.community_logo')).toBeInTheDocument();
    expect(container.queryByText('community.change_community_logo')).toBeInTheDocument();
    expect(container.queryByText('community.upload_logo')).toBeInTheDocument();
    expect(container.queryByText('community.support_contact')).toBeInTheDocument();
    expect(container.queryByText('community.make_changes_support_contact')).toBeInTheDocument();
    expect(container.queryByText('common:form_fields.add_phone_number')).toBeInTheDocument();
    expect(container.queryByText('common:form_fields.add_email_address')).toBeInTheDocument();
    expect(container.queryByText('common:form_fields.add_whatsapp_number')).toBeInTheDocument();
    expect(container.queryByText('common:form_fields.add_social_link')).toBeInTheDocument();
    expect(container.queryByText('community.update_community')).toBeInTheDocument();
    expect(container.queryByText('community.update_community')).not.toBeDisabled();
    expect(container.queryAllByLabelText('common:form_fields.email')).toHaveLength(2);
    expect(container.queryByLabelText('common:form_fields.phone_number')).toBeInTheDocument();
    expect(container.queryByLabelText('WhatsApp')).toBeInTheDocument();

    expect(container.queryAllByLabelText('remove')).toHaveLength(5);

    fireEvent.click(container.queryAllByTestId('add_number')[0]);

    expect(container.queryAllByLabelText('remove')).toHaveLength(6);
    fireEvent.click(container.queryAllByLabelText('remove')[0]);
    expect(container.queryAllByLabelText('remove')).toHaveLength(5);

    expect(container.queryByTestId('locale')).toBeInTheDocument();
    expect(container.queryByTestId('currency')).toBeInTheDocument();

    expect(container.queryByTestId('accountName')).toBeInTheDocument();
    expect(container.queryByTestId('accountNo')).toBeInTheDocument();
    expect(container.queryByTestId('bankName')).toBeInTheDocument();
    expect(container.queryByTestId('branch')).toBeInTheDocument();
    expect(container.queryByTestId('swiftCode')).toBeInTheDocument();
    expect(container.queryByTestId('sortCode')).toBeInTheDocument();
    expect(container.queryByTestId('address')).toBeInTheDocument();
    expect(container.queryByTestId('city')).toBeInTheDocument();
    expect(container.queryByTestId('country')).toBeInTheDocument();
    expect(container.queryByTestId('taxIdNo')).toBeInTheDocument();
    
    fireEvent.select(container.queryByTestId('locale'), { target: { value: 'en-US' } });
    expect(container.queryByTestId('locale').value).toBe('en-US');

    fireEvent.select(container.queryByTestId('currency'), { target: { value: 'ZMW' } });
    expect(container.queryByTestId('currency').value).toBe('ZMW');

    fireEvent.select(container.queryByTestId('accountName'), { target: { value: 'Thebe' } });
    expect(container.queryByTestId('accountName').value).toBe('Thebe');

    fireEvent.select(container.queryByTestId('accountNo'), { target: { value: '1234' } });
    expect(container.queryByTestId('accountNo').value).toBe('1234');

    fireEvent.select(container.queryByTestId('bankName'), { target: { value: 'UBA' } });
    expect(container.queryByTestId('bankName').value).toBe('UBA');

    fireEvent.select(container.queryByTestId('branch'), { target: { value: 'LU' } });
    expect(container.queryByTestId('branch').value).toBe('LU');

    fireEvent.select(container.queryByTestId('swiftCode'), { target: { value: 'xyz' } });
    expect(container.queryByTestId('swiftCode').value).toBe('xyz');

    fireEvent.select(container.queryByTestId('sortCode'), { target: { value: '067' } });
    expect(container.queryByTestId('sortCode').value).toBe('067');

    fireEvent.select(container.queryByTestId('address'), { target: { value: '11 Sub' } });
    expect(container.queryByTestId('address').value).toBe('11 Sub');

    fireEvent.select(container.queryByTestId('city'), { target: { value: 'woodlands' } });
    expect(container.queryByTestId('city').value).toBe('woodlands');

    fireEvent.select(container.queryByTestId('country'), { target: { value: 'zambia' } });
    expect(container.queryByTestId('country').value).toBe('zambia');

    fireEvent.select(container.queryByTestId('taxIdNo'), { target: { value: '432' } });
    expect(container.queryByTestId('taxIdNo').value).toBe('432');

    fireEvent.click(container.queryByTestId('email_click'));
    expect(container.queryAllByLabelText('common:form_fields.email')).toHaveLength(3);

    fireEvent.click(container.queryByTestId('whatsapp_click'));
    expect(container.queryAllByLabelText('WhatsApp')).toHaveLength(2);

    fireEvent.click(container.queryByTestId('social_link_click'));
    expect(container.queryAllByLabelText('common:form_fields.social_link')).toHaveLength(2);

    fireEvent.change(container.queryByTestId('logo_url'), {
      target: { value: 'https://something.com' }
    });
    expect(container.queryByTestId('logo_url').value).toBe('https://something.com');

    fireEvent.change(container.queryByTestId('tagline'), {
      target: { value: 'This is our tagline' }
    });
    expect(container.queryByTestId('tagline').value).toBe('This is our tagline');

    fireEvent.change(container.queryByTestId('wp_link'), {
      target: { value: 'https://wordpress.com' }
    });
    expect(container.queryByTestId('wp_link').value).toBe('https://wordpress.com');

    // fire the mutation update_community
    expect(container.queryByTestId('update_community')).not.toBeDisabled();

    fireEvent.click(container.queryByTestId('update_community'));

    expect(container.queryByTestId('update_community')).toBeDisabled();

    await waitFor(() => {
      expect(refetchMock).toBeCalled();
      expect(container.queryByText('community.community_updated')).toBeInTheDocument();
    }, 10);
  });
});
