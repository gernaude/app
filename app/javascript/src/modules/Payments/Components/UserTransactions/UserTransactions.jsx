/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import { useMutation } from 'react-apollo';
import PropTypes from 'prop-types';
import { MoreHorizOutlined } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import DataList from '../../../../shared/list/DataList';
import Text, { GridText } from '../../../../shared/Text';
import { dateToString } from '../../../../components/DateContainer';
import CenteredContent from '../../../../components/CenteredContent';
import { formatMoney, formatError } from '../../../../utils/helpers';
import MessageAlert from "../../../../components/MessageAlert"
import MenuList from '../../../../shared/MenuList'
import { TransactionRevert } from '../../graphql/payment_mutations';
import DeleteDialogueBox from '../../../../shared/dialogs/DeleteDialogue';

export default function UserTransactionsList({transaction, currencyData, userType, userData, refetch, balanceRefetch }) {
  const { t } = useTranslation('common')
  const [transactionId, setTransactionId] = useState(false)
  const [name, setName] = useState('')
  const [revertModalOpen, setRevertModalOpen] = useState(false)
  const [revertTransactionLoading, setRevertTransactionLoading] = useState(false)
  const [anchorEl, setAnchorEl] = useState(null)
  const [messageAlert, setMessageAlert] = useState('')
  const anchorElOpen = Boolean(anchorEl)
  const [isSuccessAlert, setIsSuccessAlert] = useState(false)
  const [revertTransaction] = useMutation(TransactionRevert)

  const transactionHeader = [
    { title: 'Date', value: t('common:table_headers.date'), col: 1 },
    { title: 'Recorded by', value: t('common:table_headers.recorded_by'), col: 1 },
    { title: 'Payment Type', value: t('common:table_headers.payment_type'), col: 2 },
    { title: 'Amount Paid', value: t('common:table_headers.amount_paid'), col: 1 },
    { title: 'Menu', value: t('common:table_headers.menu'), col: 1 }
  ];

  function handleClick(event, txn, user){
    const txnId = txn.id
    const userName = user.name
    event.stopPropagation()
    setTransactionId(txnId)
    setName(userName)
    setRevertModalOpen(true)
  }

  function handleRevertClose(event){
    event.stopPropagation()
    setRevertModalOpen(false)
    setRevertTransactionLoading(false)
  }

  function handleTransactionMenu(event){
    event.stopPropagation()
    setAnchorEl(event.currentTarget)
  }

  function handleRevertTransaction(event) {
    event.stopPropagation()
    setRevertTransactionLoading(true)
    revertTransaction({
      variables: {
        id: transactionId
      }
    }).then(() => {
      setAnchorEl(null)
      setMessageAlert('Transaction reverted')
      balanceRefetch()
      refetch()
      setIsSuccessAlert(true)
      setRevertModalOpen(false)
      setRevertTransactionLoading(false)
    })
    .catch((err) => {
      setMessageAlert(formatError(err.message))
      setIsSuccessAlert(false)
      setRevertTransactionLoading(false)
    })
  }

  const menuList = [
    { content: t("common:menu.revert_transaction"), isAdmin: true, color: 'red', handleClick: (event) => handleClick(event, transaction, userData)},
  ]

  function handleMessageAlertClose(_event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setMessageAlert('');
  }

  const menuData = {
    menuList,
    handleTransactionMenu,
    anchorEl,
    open: anchorElOpen,
    userType,
    handleClose: () => setAnchorEl(null)
  }

  if (!Object.keys(transaction).length || Object.keys(transaction).length === 0) {
    return <CenteredContent><Text content="No Transactions Yet" align="justify" /></CenteredContent>
  }

  return (
    <div>
      <MessageAlert
        type={isSuccessAlert ? 'success' : 'error'}
        message={messageAlert}
        open={!!messageAlert}
        handleClose={handleMessageAlertClose}
      />
      <DeleteDialogueBox
        open={revertModalOpen}
        handleClose={(event) => handleRevertClose(event)}
        handleAction={(event) => handleRevertTransaction(event)}
        title='Transaction'
        action='delete'
        user={name}
        loading={revertTransactionLoading}
      />
      <DataList
        keys={transactionHeader}
        data={[renderTransactions(transaction, currencyData, menuData)]}
        hasHeader={false}
        color
      />
    </div>
  )
}

export function renderTransactions(transaction, currencyData, menuData) {
  return {
    'Date': (
      <GridText
        data-testid="date"
        content={dateToString(transaction.createdAt)}
      />
    ),
    'Recorded by': (
      <GridText
        data-testid="recorded"
        content={transaction.depositor.name}
      />
    ),
    "Payment Type": (
      <GridText
        data-testid="description"
        content={transaction.source}
      />
    ),
    "Amount Paid": (
      <Grid item xs={12} md={2} data-testid="amount">
        <Text content={formatMoney(currencyData, transaction.allocatedAmount)} />
        <br />
        <Text color="primary" content={`unallocated ${formatMoney(currencyData, transaction.unallocatedAmount)}`} />
      </Grid>
    ),
    Menu: (
      <Grid item xs={12} md={1} data-testid="menu">
        {
          transaction.status !== 'cancelled' &&
          (
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              data-testid="menu"
              onClick={(event) => menuData.handleTransactionMenu(event)}
            >
              <MoreHorizOutlined />
            </IconButton>
          )
        }
        <MenuList
          open={menuData.open}
          anchorEl={menuData.anchorEl}
          userType={menuData.userType}
          handleClose={menuData.handleClose}
          list={menuData.menuList}
        />
      </Grid>
    )
  };
}

UserTransactionsList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  transaction: PropTypes.object.isRequired,
  currencyData: PropTypes.shape({
    currency: PropTypes.string,
    locale: PropTypes.string
  }).isRequired,
  userData: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired,
  userType: PropTypes.string.isRequired,
  refetch: PropTypes.func.isRequired,
  balanceRefetch: PropTypes.func.isRequired
};
