/* eslint-disable no-use-before-define */
import React, { Fragment, useEffect, useState } from 'react'
import { useLazyQuery, useMutation } from 'react-apollo'
import AddBoxIcon from '@material-ui/icons/AddBox'
import { useTranslation } from 'react-i18next';
import Tooltip from '@material-ui/core/Tooltip'
import { css, StyleSheet } from 'aphrodite'
import PropTypes from 'prop-types'
import { IconButton } from '@material-ui/core'
import dateutil from '../../../utils/dateutil'
import { UpdateNote } from '../../../graphql/mutations'
import { UserNotesQuery } from '../../../graphql/queries'
import { Spinner } from '../../../shared/Loading'

export function UserNote({ note, handleFlagNote }) {
  const { t } = useTranslation('users')
  return (
    <Fragment key={note.id}>
      <div className={css(styles.commentBox)}>
        <p className="comment">{note.body}</p>
        <i>
          {t("common:misc.created_at")}
          :
          {dateutil.formatDate(note.createdAt)}
        </i>
      </div>

      <Tooltip title={t("common:form_placeholders.flag")}>
        <IconButton 
          aria-label="Flag as a todo" 
          onClick={() => handleFlagNote(note.id)}
          className={css(styles.actionIcon)}
          color="primary"
        >
          <AddBoxIcon />
        </IconButton>
      </Tooltip>
      <br />
    </Fragment>
  )
}

export default function UserNotes({ userId, tabValue }){
  const [isLoading, setLoading] = useState(false)
  const [noteUpdate] = useMutation(UpdateNote)
  const [loadNotes, { loading, error, refetch, data }] = useLazyQuery(UserNotesQuery, {
    variables: { userId },
    fetchPolicy: 'cache-and-network'
  })

  useEffect(() => {
    if (tabValue === 'Notes') {
      loadNotes()
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabValue])

  function handleFlagNote(id) {
    setLoading(true)
    noteUpdate({ variables: { id, flagged: true } }).then(() => {
      setLoading(false)
      refetch()
    })
  }

  if (loading || isLoading || !data) return <Spinner />
  if (error) return error.message

  return data?.userNotes.map(note => (
    <UserNote
      key={note.id}
      note={note}
      handleFlagNote={handleFlagNote}
    />
  ))
}


UserNote.propTypes = {
  note: PropTypes.shape({
    id: PropTypes.string,
    completed: PropTypes.bool,
    createdAt: PropTypes.string,
    body: PropTypes.string,
    flagged: PropTypes.bool
  }).isRequired,
  handleFlagNote: PropTypes.func.isRequired,
}

UserNotes.propTypes = {
  userId: PropTypes.string.isRequired
}

const styles = StyleSheet.create({
  commentBox: {
    borderLeft: '2px solid',
    padding: '0.5%',
  },
  actionIcon: {
    float: 'right',
    cursor: 'pointer',
    marginRight: 12
  }
})
