<!-- > Thank you for contributing to DoubleGDP source code -->

**Describe changes made and the chosen implementation strategy**

<!-- remove this comment and  explain here -->

**Issue number this closes**

**Does this merge request introduce any new gem or package**

<!-- If Yes add a link here -->

- [ ] Merge Request does not include breaking changes
- [ ] I have tested and verified security guard's flow works fine
- [ ] I have tested and verified event logs works fine
- [ ] I have linked the source and name of any newly added gem or node_module package

