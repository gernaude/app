# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::EntryRequest do
  describe 'creating an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:admin) { create(:admin_user, community_id: user.community_id) }
    let!(:contractor) { create(:contractor, community_id: user.community_id) }

    let(:query) do
      <<~GQL
        mutation CreateEntryRequest($name: String!, $reason: String!, $temperature: String) {
          result: entryRequestCreate(name: $name, reason: $reason, temperature: $temperature) {
            entryRequest {
              id
              name
              user {
                id
              }
            }
          }
        }
      GQL
    end

    it 'returns a created entry request' do
      variables = {
        name: 'Mark Percival',
        reason: 'Visiting',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: user,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      expect(result['errors']).to be_nil
    end

    it 'returns Unauthorized for non Unauthorized users' do
      variables = {
        name: 'Mark Percival',
        reason: 'Visiting',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: nil,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'Unauthorized'
    end

    it 'if temperature is provided it should create a user_temp event' do
      variables = {
        name: 'Mark Percival',
        reason: 'Visiting',
        temperature: '30',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: user,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      ref_id = result.dig('data', 'result', 'entryRequest', 'id')
      log = Logs::EventLog.find_by(ref_id: ref_id)
      expect(log.ref_type).to eql 'Logs::EntryRequest'
      expect(log.data['note']).to eql '30'
      expect(log.subject).to eql 'user_temp'
      expect(result['errors']).to be_nil
    end
  end

  describe 'updating an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:admin) { create(:admin_user, community_id: user.community_id) }
    let!(:entry_request) { user.entry_requests.create(name: 'Mark Percival', reason: 'Visiting') }

    let(:query) do
      <<~GQL
        mutation UpdateEntryRequest($id: ID!, $name: String) {
          result: entryRequestUpdate(id: $id, name: $name) {
            entryRequest {
              id
              name
            }
          }
        }
      GQL
    end

    it 'returns an updated entry request' do
      variables = {
        id: entry_request.id,
        name: 'Mark Smith',
        reason: 'Visiting',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: user,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      expect(result.dig('data', 'result', 'entryRequest', 'name')).to eql 'Mark Smith'
      expect(result['errors']).to be_nil
    end
  end

  describe 'denying an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:admin) { create(:admin_user, community_id: user.community_id) }
    let!(:entry_request) { admin.entry_requests.create(name: 'Mark Percival', reason: 'Visiting') }

    let(:query) do
      <<~GQL
        mutation UpdateEntryRequest($id: ID!) {
          result: entryRequestDeny(id: $id) {
            entryRequest {
              id
              name
              grantedState
            }
          }
        }
      GQL
    end

    it 'returns a denied entry request' do
      variables = {
        id: entry_request.id,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: admin,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      expect(result.dig('data', 'result', 'entryRequest', 'grantedState')).to eql 2
      expect(result['errors']).to be_nil
    end

    it 'returns not found when a request does not exist' do
      variables = {
        id: SecureRandom.uuid,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: admin,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'Logs::EntryRequest'
    end
  end

  describe 'granting an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:another_user) { create(:user, community_id: user.community_id) }
    let!(:admin) { create(:admin_user, community_id: user.community_id) }
    let!(:entry_request) { admin.entry_requests.create(name: 'Mark Percival', reason: 'Visiting') }
    let!(:event) do
      user.generate_events('visitor_entry', entry_request)
    end
    let!(:contractor) { create(:contractor, community_id: user.community_id) }

    let(:query) do
      <<~GQL
        mutation UpdateEntryRequest($id: ID!) {
          result: entryRequestGrant(id: $id) {
            entryRequest {
              id
              name
              grantedState
            }
          }
        }
      GQL
    end

    it 'returns a granted entry request' do
      variables = {
        id: event.ref_id,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: admin,
                                                site_community: user.community,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      expect(result.dig('data', 'result', 'entryRequest', 'grantedState')).to eql 1
      expect(result['errors']).to be_nil
    end

    it 'returns Unauthorized for non Unauthorized users' do
      variables = {
        id: event.ref_id,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: contractor,
                                                site_community: user.community,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'Unauthorized'
    end

    it 'returns not found for wrong events' do
      variables = {
        id: SecureRandom.uuid,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: admin,
                                                site_community: user.community,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'Event log not found'
    end
  end

  describe 'acknowledging an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:admin) { create(:admin_user, community_id: user.community_id) }
    let!(:entry_request) { admin.entry_requests.create(name: 'Mark Percival', reason: 'Visiting') }

    let(:query) do
      <<~GQL
        mutation EntryRequestAcknowledgeMutation($id: ID!) {
          result: entryRequestAcknowledge(id: $id) {
            entryRequest {
              id
              acknowledged
            }
          }
        }
      GQL
    end

    it 'returns an acknowledged entry request' do
      variables = {
        id: entry_request.id,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: admin,
                                              }).as_json
      expect(result.dig('data', 'result', 'entryRequest', 'id')).not_to be_nil
      expect(result.dig('data', 'result', 'entryRequest', 'acknowledged')).to eql true
      expect(result['errors']).to be_nil
    end
  end

  describe 'adding an observation note to an entry request' do
    let!(:user) { create(:user_with_community) }
    let!(:guard) { create(:security_guard, community_id: user.community_id) }
    let!(:contractor) { create(:contractor, community_id: user.community_id) }
    let!(:entry_request) { guard.entry_requests.create(name: 'Mark Percival', reason: 'Visiting') }
    let!(:event) do
      guard.generate_events('observation_log', entry_request)
    end

    let(:query) do
      <<~GQL
        mutation addObservationNote($id: ID, $note: String, $refType: String, $eventLogId: ID) {
          entryRequestNote(id: $id, note: $note, refType: $refType, eventLogId: $eventLogId) {
            event {
              id
              data
              refType
            }
          }
        }
      GQL
    end

    it 'adds a note to an entry request' do
      variables = {
        id: entry_request.id,
        note: 'The vehicle was too noisy',
        refType: 'Logs::EntryRequest',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: guard,
                                                site_community: guard.community,
                                              }).as_json
      expect(result['errors']).to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'id')).not_to be_nil
    end

    it 'adds a note to an user entry' do
      variables = {
        id: contractor.id,
        note: 'The user',
        refType: 'Users::User',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: guard,
                                                site_community: guard.community,
                                              }).as_json
      expect(result['errors']).to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'id')).not_to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'refType')).to eql 'Users::User'
      expect(result.dig('data', 'entryRequestNote', 'event', 'data', 'note')).to include 'The user'
    end

    it 'adds a note without any entry' do
      variables = {
        id: nil,
        note: 'An ordinary note',
        refType: nil,
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: guard,
                                                site_community: guard.community,
                                              }).as_json
      expect(result['errors']).to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'refType')).to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'data', 'note')).to include(
        'An ordinary note',
      )
    end

    it 'returns an error when note is empty' do
      variables = {
        id: contractor.id,
        note: '',
        refType: 'Users::User',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: guard,
                                                site_community: guard.community,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('data', 'entryRequestNote', 'event', 'id')).to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'cannot be empty'
    end

    it 'returns Unauthorized for non admin and security_guard' do
      variables = {
        id: entry_request.id,
        note: 'The vehicle was too noisy',
        refType: 'Logs::EntryRequest',
      }
      result = DoubleGdpSchema.execute(query, variables: variables,
                                              context: {
                                                current_user: contractor,
                                                site_community: contractor.community,
                                              }).as_json
      expect(result['errors']).not_to be_nil
      expect(result.dig('errors', 0, 'message')).to include 'Unauthorized'
    end
  end
end
